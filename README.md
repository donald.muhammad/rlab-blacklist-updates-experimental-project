# RLab Blacklist Updates Experimental Project RLab hēi míngdān gēngxīn shíyàn xiàngmù
<img style="float:right;" src="nerd-1.jpg"/>

This is yet another aggregation of curated blacklists for system security. <br/>
zhè shì lìng yīgè zhēnduì xìtǒng ānquán de jīng xuǎn hēi míngdān jùhé.<br/>

There are better lists out there, but this setup suits my life style and budget. <br/>
Nà li yǒu gèng hǎo de qīngdān, dàn zhè zhǒng shèzhì shìhé wǒ de shēnghuó fāngshìhé yùsuàn.<br/><br/>

Curated from freely available sources. Use at your own risk. <br/>
Cóng miǎnfèi tígōng de zīyuán zhōng cèhuà. Shǐyòng fēngxiǎn zìfù. <br/>

<b>This list may cause disruption to your network, or make certain websites break.</b> <br/>
<b>Cǐ lièbiǎo kěnéng huì dǎozhì nín de wǎngluò zhōngduàn, huò shǐ mǒu xiē wǎngzhàn zhōngduàn. </b><br/>

Not for production. NO WARRANTY provided.  <br/> 
Bùyòng yú shēngchǎn. Bù tígōng rènhé bǎozhèng.<br/>

## Description Miáoshù
Blacklists for hēi míngdān -
- IP Firewalls IP fánghuǒqiáng
    - Country level blockers guójiā jí lánjié qì
    - Ephemeral cidrs that are deemed malicious bèi rènwéi shì èyì de línshí cidr
- DNS Unbound Resolvers DNS wèi bǎng dìng jiěxī qì
    - Standard malware, phishing, cnc and ad domains biāozhǔn èyì ruǎnjiàn, wǎngluò diàoyú,cnc hé guǎnggào yù
    - Porn A piàn

We split the above blacklists for for business usage, and for residential usage. <br/>
wǒmen jiāng shàngshù hēi míngdān fēn wéi shāngyè yòngtú hé zhùzhái yòngtú. <br/>

This setup is ideal for work from home freelancers.<br/>
Cǐ shèzhì fēicháng shì hé zàijiā zìyóu zhíyè zhě gōngzuò.<br/>

## Installation Ānzhuāng
This repo is for advanced sys admins who know how to configure their own network security. <br/>
cǐ repo shìyòng yú zhīdào rúhé pèizhì zìjǐ de wǎngluò ānquán de gāojí xìtǒng guǎnlǐ yuán. <br/>

Updates will only be released after major global events. <br/>
Gēngxīn zhǐ huì zài zhòngdà quánqiú shìjiàn zhīhòu fābù. <br/>

This is not a repo that is actively maintained. <br/>
Zhè bùshì yīgè jījí wéihù de huí gòu. <br/>

## Quick Guide Kuàisù zhǐnán
### Common Usage Chángyòng yòngfǎ
For a typical desktop windows user, one could just move [this](http://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/blob/main/enterprise-hardware/residential/hosts) file into ```C:\Windows\System32\drivers\etc``` <br/>
Duìyú diǎnxíng de zhuōmiàn windows yònghù, kěyǐ jiāng zhège wénjiàn yídòng dào <br/>

For a linux desktop user you move it into ```/etc/hosts``` <br/>
Duìyú linux zhuōmiàn yònghù, nín kěyǐ jiāng qí yí zhì <br/>

### Freelance Usage Zìyóu shǐyòng
A typical freelancer should use the business profile [here](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/blob/main/enterprise-hardware/business/hosts) <br/>
Diǎnxíng de zìyóu zhíyè zhě yīnggāi zài cǐ chù shǐyòng yèwù zīliào <br/>

A freelancer working from home should configure their router to use the residential profiles [here ipv4](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v4/normal_profile) and [here ipv6](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v6/normal_profile) <br/>
Zàijiā gōngzuò de zìyóu zhíyè zhě yīng jiāng qí lùyóuqì pèizhì wèi shǐyòng zhùzhái pèizhì wénjiàn <br/>

A small business owner should configure device end points and their router to use the business profile [here](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/business)<br/>
Xiǎo qǐyè zhǔ yīng zài cǐ chù pèizhì shèbèi duāndiǎn jí qí lùyóuqì yǐ shǐyòng yèwù pèizhì wénjiàn<br/>

Automatic updates from insecure endpoints must be blocked by default, windows updates can be blocked [here](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/blob/main/enterprise-hardware/business/unbound-hosts-block-win.conf) <br/>
Mòrèn qíngkuàng xià bìxū zǔzhǐ láizì bù ānquán duāndiǎn de zìdòng gēngxīn, kěyǐ zǔzhǐ Windows gēngxīn <br/>

Updates should be offline via one time bulk downloads, and via a secure network. <br/>
Gēngxīn yìng tōngguò yīcì xìng pīliàng xiàzài hé ānquán wǎngluò líxiàn. <br/>

### Advanced Usage Gāojí yòngfǎ
Most people do not need full open internet. <br/>
Dà duōshù rén bù xūyào wánquán kāifàng de hùliánwǎng. <br/>

Most foreign websites use CDN edge mirrors that are served locally to a user within their country. <br/>
Dà duōshù wàiguó wǎngzhàn dōu shǐyòng CDN biānyuán jìngxiàng, zhèxiē jìngxiàng zài běndì tígōng jǐ qí guójiā/dìqū de yònghù. <br/>

An advanced user should switch client firewall profiles if they want Gāojí yònghù yīng gēnjù xūyào qiēhuàn kèhù duān fánghuǒqiáng pèizhì wénjiàn-
- to use websites that are most commonly accessed [here ipv4](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v4/normal_profile) [here ipv6](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v6/normal_profile) Shǐyòng zuì cháng fǎngwèn de wǎngzhàn
   - Blocks africa, russia and middle east Fēngsuǒ fēizhōu, èluósī hé zhōngdōng
- to use euro zone [gdpr](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) websites [here ipv4](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v4/euro_profile) [here ipv6](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v6/euro_profile) Duì yònghù ōuyuán qū gdpr wǎngzhàn
   - Blocks africa, russia and middle east, asia pacific Fēngsuǒ fēizhōu, èluósī hé zhōngdōng, yàtài dìqū
- to use global websites served from consumer countries [here ipv4](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v4/consumer_profile) [here ipv6](https://gitlab.com/donald.muhammad/rlab-blacklist-updates-experimental-project/-/tree/main/enterprise-hardware/openwrt_fw3_v6/consumer_profile) Shǐyòng láizì xiāofèi guó de quánqiú wǎngzhàn
   - Blocks africa, russia and middle east, asia pacific, euro zone Fēngsuǒ fēizhōu, èluósī hé zhōngdōng, yàtài dìqū, ōuyuán qū

Freelancers with trade secrets, tools of the trade, should consider storing it in euro zone [gdpr](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) service providers, and use the switching logic above to prevent malicious access. <br/>
Zìyóu zhíyè zhě jùyǒu shāngyè mìmì, màoyì de gōngjù, yìng kǎolǜ jiāng qí cúnchú zài ōuyuán qū fúwù tígōng shāng zhōng, bìng shǐyòng shàngmiàn de jiāohuàn luójí lái fángzhǐ èyì fǎngwèn. <br/>

Anyone dealing with sensitive data should brush up on cyber security and use the firewall and dns blacklists for all devices. <br/>
Rènhé chǔlǐ mǐngǎn shùjù de rén dōu yīnggāi liǎojiě wǎngluò ānquán, bìng wéi suǒyǒu shèbèi shǐyòng fánghuǒqiáng hé dns hēi míngdān.<br/>

## Support Zhīchí
You are the support. <br/>
nǐ shì zhīchí zhě. <br/>

## Contributing Gòngxiàn
N/A. This repo is only for reference and as an online backup.<br/>
bù shìyòng. Cǐ repo jǐn gōng cānkǎo, bìng zuòwéi zàixiàn bèifèn.<br/>

Use at your own risk.<br/>
Shǐyòng fēngxiǎn zìfù.<br/>

## Authors and acknowledgment Zuòzhě hé zhìxiè
Most of the ips and domains are curated from publically available lists... <br/>
dà duōshù ips hé yù dōu shì cóng gōngkāi kěyòng dì lièbiǎo zhōng tiāoxuǎn chūlái de......<br/>

Domain blacklists were generated using Steven Black's hosts program. <br/>
Yù hēi míngdān shì shǐyòng Steven Black de zhǔjī chéngxù shēngchéng de. <br/>

IP blacklists were sourced mostly from ipdeny.com. <br/>
IP hēi míngdān zhǔyào láizì ipdeny.Com. <br/>


Source                          | My Mirror | Original Link | Licence
------------------------------- |:---------:|:-------------:|:--------:
Steven Blacks hosts | [link](http://gitlab.com/donald.muhammad/hosts) | [link](http://github.com/StevenBlack/hosts) | MIT 
IP Deny | N/A | [link](http://www.ipdeny.com/ipblocks/) | Free
Gorhill uBlock Origin | [link](http://gitlab.com/donald.muhammad/ublock) | [link](http://github.com/gorhill/uBlock) | GNUv3
Sec OPs Institute Tor IP Address | [link](http://gitlab.com/donald.muhammad/Tor-IP-Addresses) | [link](http://github.com/SecOps-Institute/Tor-IP-Addresses) | Free
MalCode IP Blacklists | N/A | [link](http://malc0de.com) | Free
Feodo Tracker Abuse IP Blacklist | N/A | [link](http://feodotracker.abuse.ch) | Free
Spam Haus IPv4 and IPv6 Blacklists | N/A | [link](http://www.spamhaus.org) | Free
URL Haus Abuse Blacklists | N/A | [link](http://urlhaus.abuse.ch) | Free
SSL Abuse Blacklist | N/A | [link](http://sslbl.abuse.ch) | Free
Someone Who Cares Blacklist | N/A | [link](http://someonewhocares.org) | Free
Phish Tank Blacklist | N/A | [link](http://data.phishtank.com) | Free
Citizen Lab Blue Coat Investigations | [link](http://gitlab.com/donald.muhammad/bluecoat-investigations) | [link](http://github.com/citizenlab/bluecoat-investigations)| [CC ANC SA 4](http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
Amnesty Tech COVID19 Apps | [link](http://gitlab.com/donald.muhammad/covid19-apps) | [link](http://github.com/AmnestyTech/covid19-apps) | Free
Amnesty Tech Investigations | [link](http://gitlab.com/donald.muhammad/investigations) | [link](http://github.com/AmnestyTech/investigations) | [CC BY](http://creativecommons.org/licenses/by/2.0/)
Citizen Lab Malware Indicators | [link](http://gitlab.com/donald.muhammad/malware-indicators) | [link](http://github.com/citizenlab/malware-indicators) | [CC ANC SA 4](http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
Citizen Lab Spyware Scan | [link](https://gitlab.com/donald.muhammad/spyware-scan) | [link](https://github.com/citizenlab/spyware-scan) | BSD-3
Te-k Stalkerware Indicators | [link](https://gitlab.com/donald.muhammad/stalkerware-indicators) | [link](https://github.com/Te-k/stalkerware-indicators) | Free

Not all domains and IP addresses were used. Not all url sources are used. Some IPs are old and will likely yield a false positive.<br/>
Bìngfēi suǒyǒu yù hé IP dìzhǐ dōu bèi shǐyòng. Bìngfēi suǒyǒu url yuán dōu bèi shǐyòng. Yǒuxiē IP hěn jiù, hěn kěnéng huì chǎnshēng wù bào. <br/>

A source will only be used if it passes my multi stage cyber security assurance check, on a custom hardened immutable linux machine. <br/>
Zhǐyǒu dāng tā tōngguò wǒ de duō jiēduàn wǎngluò ānquán bǎozhèng jiǎnchá shí, cái huì zài zì dìngyì qiánghuà bùkě biàn linux jīqì shàng shǐyòng yuán.<br/>


